import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import { Home } from "./components/Home";
import { AddUser } from "./components/AddUser";
import { Edituser } from "./components/EditUser";
import { GlobalProvider } from "./context/GlobalState";
import UserSearch from "./components/UserSearch";

import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  const [searchTerm, setSearchTerm] = useState("");
  return (
    <div style={{ maxWidth: "30rem", margin: "4rem auto" }}>
      <GlobalProvider>
        <Router>
          <UserSearch searchTerm={searchTerm} setSearchTerm={setSearchTerm} /> {/* Include the UserSearch component */}
          <Switch>
            <Route exact path="/" render={(props) => <Home searchTerm={searchTerm} />} />
            <Route exact path="/add" component={AddUser} />
            <Route exact path="/edit/:id" component={Edituser} />
          </Switch>
        </Router>
      </GlobalProvider>
    </div>
  );
};

export default App;
