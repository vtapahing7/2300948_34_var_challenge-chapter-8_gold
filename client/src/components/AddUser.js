import React, { useState, useContext } from "react";
import { GlobalContext } from "../context/GlobalState";
import { v4 as uuid } from "uuid";
import { Link, useHistory } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";

export const AddUser = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState(""); // Initialize email as an empty string
  const [experience, setExperience] = useState(0);
  const [level, setLevel] = useState(0);
  const { addUser } = useContext(GlobalContext);
  const history = useHistory();

  // onSubmit
  const onSubmit = (e) => {
    e.preventDefault();
    const newUser = {
      id: uuid(),
      name,
      email, // Include email in the newUser object
      experience,
      level,
    };
    addUser(newUser);
    history.push("/");
  };

  // onChange for name
  const onNameChange = (e) => {
    setName(e.target.value);
  };

  // onChange for email
  const onEmailChange = (e) => {
    setEmail(e.target.value);
  };

  // onChange for experience
  const onExperienceChange = (e) => {
    setExperience(parseInt(e.target.value, 10));
  };

  // onChange for level
  const onLevelChange = (e) => {
    setLevel(parseInt(e.target.value, 10));
  };

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label>Name</Label>
        <Input
          type="text"
          value={name}
          onChange={onNameChange}
          name="name"
          placeholder="Enter a name"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Email</Label>
        <Input
          type="email"
          value={email}
          onChange={onEmailChange}
          name="email"
          placeholder="Enter an email"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Experience</Label>
        <Input
          type="number"
          value={experience}
          onChange={onExperienceChange}
          name="experience"
          placeholder="Enter experience"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Level</Label>
        <Input
          type="number"
          value={level}
          onChange={onLevelChange}
          name="level"
          placeholder="Enter level"
          required
        />
      </FormGroup>
      <Button type="submit">Submit</Button>
      <Link to="/" className="btn btn-danger ml-2">
        Cancel
      </Link>
    </Form>
  );
};
