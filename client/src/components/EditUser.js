import React, { useState, useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalState";
import { Link, useHistory } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";

export const Edituser = (props) => {
  const { editUser, users } = useContext(GlobalContext);
  const [selectedUser, setSelectedUser] = useState({
    id: "",
    name: "",
    email: "",
    experience: 0, // Initialize experience with 0
    level: 0, // Initialize level with 0
  });
  const history = useHistory();
  const currentUserId = props.match.params.id;

  useEffect(() => {
    const userId = currentUserId;
    const selectedUser = users.find((user) => user.id === userId);
    setSelectedUser(selectedUser);
  }, [currentUserId, users]);

  const onChange = (e) => {
    setSelectedUser({ ...selectedUser, [e.target.name]: e.target.value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    editUser(selectedUser);
    history.push("/");
  };

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label>Name</Label>
        <Input
          type="text"
          value={selectedUser.name}
          onChange={onChange}
          name="name"
          placeholder="Enter a name"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Email</Label>
        <Input
          type="email"
          value={selectedUser.email}
          onChange={onChange}
          name="email"
          placeholder="Enter an email"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Experience</Label>
        <Input
          type="number"
          value={selectedUser.experience}
          onChange={onChange}
          name="experience"
          placeholder="Enter experience"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label>Level</Label>
        <Input
          type="number"
          value={selectedUser.level}
          onChange={onChange}
          name="level"
          placeholder="Enter level"
          required
        />
      </FormGroup>
      <Button type="submit">Edit User</Button>
      <Link to="/" className="btn btn-danger ml-2">
        Cancel
      </Link>
    </Form>
  );
};
