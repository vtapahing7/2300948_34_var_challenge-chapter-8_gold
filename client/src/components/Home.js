import React from "react";
import { Heading } from "./Heading";
import { UserList } from "./UserList";

export const Home = ({ searchTerm }) => {
  return (
    <>
      <Heading />
      <UserList searchTerm={searchTerm} /> {/* Pass searchTerm to UserList */}
    </>
  );
};
