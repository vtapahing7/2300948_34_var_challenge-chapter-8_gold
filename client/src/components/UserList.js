import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem, Button } from "reactstrap";

export const UserList = ({ searchTerm }) => {
  const { users, removeUser } = useContext(GlobalContext);

  // Filter the users based on the searchTerm
  const filteredUsers = users.filter((user) => {
    if (!searchTerm) return true; // If searchTerm is empty, show all users

    const lowerCaseSearchTerm = searchTerm.toLowerCase();

    // Add checks to ensure that properties are defined before calling toLowerCase
    return (
      (user.username && user.username.toLowerCase().includes(lowerCaseSearchTerm)) ||
      (user.email && user.email.toLowerCase().includes(lowerCaseSearchTerm)) ||
      (user.experience && user.experience.toLowerCase().includes(lowerCaseSearchTerm)) ||
      (user.level && user.level.toLowerCase().includes(lowerCaseSearchTerm))
    );
  });

  return (
    <ListGroup className="mt-4">
      {filteredUsers.length > 0 ? (
        <>
          {filteredUsers.map((user) => (
            <ListGroupItem className="d-flex" key={user.id}>
              <div>
                <strong>Name: {user.name}</strong>
                <p>Email: {user.email}</p>
                <p>Experience: {user.experience}</p>
                <p>Level: {user.level}</p>
              </div>
              <div className="ml-auto">
                <Link
                  to={`/edit/${user.id}`}
                  color="warning"
                  className="btn btn-warning mr-1"
                >
                  Edit
                </Link>
                <Button onClick={() => removeUser(user.id)} color="danger">
                  Delete
                </Button>
              </div>
            </ListGroupItem>
          ))}
        </>
      ) : (
        <h4 className="text-center">No Users</h4>
      )}
    </ListGroup>
  );
};
