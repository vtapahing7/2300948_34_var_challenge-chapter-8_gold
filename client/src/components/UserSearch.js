import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";

const UserSearch = ({ searchTerm, setSearchTerm }) => {
  const { searchUsers } = useContext(GlobalContext);

  const handleSearch = () => {
    searchUsers(searchTerm.toLowerCase());
  };

  return (
    <div className="mb-3">
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search by username, email, experience, or level"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)} 
        />
        <button className="btn btn-primary" onClick={handleSearch}>
          Search
        </button>
      </div>
    </div>
  );
};

export default UserSearch;
