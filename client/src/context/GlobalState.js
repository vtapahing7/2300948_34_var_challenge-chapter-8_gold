import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";

// Intial state
const initialState = {
  users: [],
  filteredUsers: [],
};

// Create context
export const GlobalContext = createContext(initialState);

// Provider component
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  // Actions
  const searchUsers = (searchTerm) => {
    const filteredUsers = state.users.filter(
      (user) =>
        (user.username && user.username.toLowerCase().includes(searchTerm)) ||
        (user.email && user.email.toLowerCase().includes(searchTerm)) ||
        (user.experience &&
          user.experience.toLowerCase().includes(searchTerm)) ||
        (user.level && user.level.toLowerCase().includes(searchTerm))
    );

    dispatch({
      type: "SEARCH_USERS", 
      payload: filteredUsers });
  };

  const removeUser = (id) => {
    dispatch({
      type: "REMOVE_USER",
      payload: id,
    });
  };

  const addUser = (user) => {
    dispatch({
      type: "ADD_USER",
      payload: user,
    });
  };

  const editUser = (user) => {
    dispatch({
      type: "EDIT_USER",
      payload: user,
    });
  };

  return (
    <GlobalContext.Provider
      value={{
        users: state.users,
        removeUser,
        addUser,
        editUser,
        filteredUsers: state.filteredUsers,
        searchUsers,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
